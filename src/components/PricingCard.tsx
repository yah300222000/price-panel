import "./PricingCard.scss";
import { PriceOption } from "../redux/price/state";
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonItem,
  IonLabel,
  IonIcon,
  IonNote,
  IonText,
} from "@ionic/react";
import { checkmark, close } from "ionicons/icons";
import { IonButton } from "@ionic/react";

const labels: any = {
  general: "General",
  specialist: "Specialist",
  physiotherapy: "Physiotherapy",
  daily_room_allowance: "Room and board (Daily)",
  daily_doctor_allowance: "Doctor's visit fee (Daily)",
  complex: "Complex",
  major: "Major",
  intermediate: "Intermediate",
  minor: "Minor",
};

function checkLabels(key: string): string | undefined {
  for (let label in labels) {
    if (label == key) {
      return labels[label];
    }
  }
}

const PricingCard: React.FC<{
  priceOptions: PriceOption;
  clickToggle?: (name: string, price:number) => void;
}> = (props) => {
  const { priceOptions } = props;
  // console.log("priceOptions at PricingTable", priceOptions);

  return (
    // <= 575px width - col: 12/12
    // > 575px <= 767px width - col: 6/12
    // > 767px <= 991px width - col: 6/12
    // > 991px <= 1199px width - col: 6/12
    // > 1199px width - col: 4/12
    <IonCol size-xs="12" size-sm="6" size-md="6" size-lg="6" size-xl="4">
      <IonCard className="pricing-card">
        <IonCardHeader>
          <IonCardTitle>{priceOptions.name}</IonCardTitle>
        </IonCardHeader>
        <IonCardContent>
          {Object.entries(priceOptions).map(([key, value], index: number) => {
            // boolean type item sticks with a tick or cross symbol
            if (typeof value == "boolean") {
              return (
                <IonItem lines="full" key={index}>
                  <IonLabel>{checkLabels(key)}</IonLabel>
                  <IonIcon src={value ? checkmark : close} />
                </IonItem>
              );
            }

            // number type item sticks with price/dollar sign
            if (typeof value == "number" && key !== "price") {
              let priceString = value.toLocaleString("zh-HK");
              return (
                <IonItem lines="full" key={index} className="price-item">
                  <IonLabel>{checkLabels(key)}</IonLabel>
                  <IonText>HK$ {priceString}</IonText>
                </IonItem>
              );
            }
          })}
          <IonButton
            expand="block"
            color="secondary"
            onClick={() => {
              props.clickToggle!(priceOptions.name, priceOptions.price);
            }}
          >
            HK$ {priceOptions.price} /month
          </IonButton>
        </IonCardContent>
      </IonCard>
    </IonCol>
  );
};

export default PricingCard;
