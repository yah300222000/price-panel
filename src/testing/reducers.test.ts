import { PriceState } from '../redux/price/state';
import { addPriceAction } from '../redux/price/actions';
import { priceReducers } from '../redux/price/reducers';

describe('Reducer', ()=>{
    let initialState: PriceState

    beforeEach(()=>{
        initialState={
            priceOptions: [
                {
                  name: "Standard Plan",
                  price: 109,
                  general: true,
                  specialist: false,
                  physiotherapy: false,
                  daily_room_allowance: 750,
                  daily_doctor_allowance: 750,
                  complex: 50000,
                  major: 25000,
                  intermediate: 12500,
                  minor: 5000,
                }
            ]
        }
    })

    it('should include the 2nd price option', ()=>{
        const nextState = priceReducers(initialState, addPriceAction(
            {
                name: "Premium Plan",
                price: 181,
                general: true,
                specialist: true,
                physiotherapy: true,
                daily_room_allowance: 1100,
                daily_doctor_allowance: 960,
                complex: 50000,
                major: 25000,
                intermediate: 12500,
                minor: 5000,
              }
        ))
        expect(nextState.priceOptions.map(option=>option.name)).toContain("Premium Plan")
    })
    
    
})