import '@testing-library/jest-dom'
import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import PricingCard from '../components/PricingCard'
import { PriceOption } from "../redux/price/state";
import { shallow, configure } from 'enzyme';
import { IonItem, IonButton } from '@ionic/react';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('PricingCard component', ()=>{
    let option: PriceOption
    beforeEach(()=>{
        option={
            name: "Premium Plus",
            price: 323,
            general: true,
            specialist: true,
            physiotherapy: true,
            daily_room_allowance: 750,
            daily_doctor_allowance: 750,
            complex: 75000,
            major: 37500,
            intermediate: 18750,
            minor: 7500,
        }
    })

    it('should render the 1st option with initial props', async ()=>{
        const wrapper = shallow(<PricingCard priceOptions={option}/>)
        expect(wrapper.find(IonItem)).toHaveLength(9)
        expect(wrapper.find(IonButton).text()).toEqual('HK$ 323 /month');
    })
    
})