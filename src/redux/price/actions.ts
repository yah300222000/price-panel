import { PriceOption } from "./state";


export function failedAction(msg: string) {
  return {
    type: "@@Price/failed" as const,
    msg,
  };
}

export function addPriceAction(priceOption:PriceOption) {
  return {
    type: "@@Price/addPrice" as const,
    priceOption,
  };
}


export type PriceAction =
  | ReturnType<typeof addPriceAction>
  | ReturnType<typeof failedAction>;
