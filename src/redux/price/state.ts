export interface PriceOption {
  name: string;
  price: number;
  general: boolean;
  specialist: boolean;
  physiotherapy: boolean;

  // hospital charge
  daily_room_allowance: number;
  daily_doctor_allowance: number;

  // surgeon fee
  complex: number;
  major: number;
  intermediate: number;
  minor: number;
}

export type PriceState = {
  priceOptions: PriceOption[];
};
