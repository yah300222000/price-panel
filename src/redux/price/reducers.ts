import { PriceState } from "./state";
import { PriceAction } from "./actions";

// simplified price options ref from Bowtie
const initialState: PriceState = {
  priceOptions: [
    {
      name: "Standard Plan",
      price: 109,
      general: true,
      specialist: false,
      physiotherapy: false,
      daily_room_allowance: 750,
      daily_doctor_allowance: 750,
      complex: 50000,
      major: 25000,
      intermediate: 12500,
      minor: 5000,
    },
    {
      name: "Premium Plan",
      price: 181,
      general: true,
      specialist: true,
      physiotherapy: true,
      daily_room_allowance: 1100,
      daily_doctor_allowance: 960,
      complex: 50000,
      major: 25000,
      intermediate: 12500,
      minor: 5000,
    },
    {
      name: "Premium Plus",
      price: 323,
      general: true,
      specialist: true,
      physiotherapy: true,
      daily_room_allowance: 750,
      daily_doctor_allowance: 750,
      complex: 75000,
      major: 37500,
      intermediate: 18750,
      minor: 7500,
    },
  ],
};

export let priceReducers = (
  state: PriceState = initialState,
  action: PriceAction
): PriceState => {
  switch (action.type) {
    case "@@Price/addPrice":
      let newPriceOptions = Array.from(state.priceOptions);
      newPriceOptions.push(action.priceOption);
      return {
        priceOptions: newPriceOptions,
      };
    case "@@Price/failed":
      return {
        priceOptions: [],
      };
    default:
      return state;
  }
};
