import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { PriceState } from "./price/state";
import { PriceAction } from "./price/actions";
import { priceReducers } from "./price/reducers";

// unified redux state
export interface IRootState {
    price: PriceState;
}

// unified redux action
export type IRootAction =
    | PriceAction

// unified redux reducers
const rootReducer = combineReducers<IRootState>({
    price: priceReducers
});

export let store = createStore<IRootState, IRootAction, {}, {}>(
    rootReducer
);
