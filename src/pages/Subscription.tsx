import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonRow,
  IonTitle,
  IonToast,
  IonToolbar,
} from "@ionic/react";
import { useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import {useState} from 'react';
import PricingCard from "../components/PricingCard";


type ToastState = {
  color: string;
  msg: string;
  duration: number;
};

const Subscription: React.FC = () => {
  const priceOptions = useSelector(
    (state: IRootState) => state.price.priceOptions
  );
  const [toast, setToast] = useState<ToastState | null>(null);
  // console.log('priceOptions at Subscription', priceOptions)
  function toggleToast(name:string, price:number){
    setToast({
      color: "success",
      msg: `You have selected ${name} - HK$ ${price} /month`,
      duration: 2000,
    });
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Subscription</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent style={{ overflowX: "scroll" }}>
        {/* <PricingTable priceOptions={priceOptions}/> */}
        <IonRow>
          {priceOptions
            ? priceOptions.map((option, index) => (
                <PricingCard priceOptions={option} clickToggle={toggleToast} key={index} />
              ))
            : ""}
        </IonRow>
        <IonToast
          isOpen={!!toast}
          onDidDismiss={() => setToast(null)}
          message={toast ? toast.msg : ""}
          duration={toast ? toast.duration : 0}
          position="top"
          color={toast ? toast.color : "medium"}
        />
      </IonContent>
    </IonPage>
  );
};

export default Subscription;
