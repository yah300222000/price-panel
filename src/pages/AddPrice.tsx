import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonList,
  IonItemDivider,
  IonItem,
  IonTextarea,
  IonLabel,
  IonButton,
  IonInput,
  IonCheckbox,
  IonToast,
} from "@ionic/react";
import { Form } from "reactstrap";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { addPriceAction } from "../redux/price/actions";
import { IonAlert, IonCol, IonRow } from "@ionic/react";
import { useState } from "react";
import { PriceOption } from "../redux/price/state";

type AlertState = {
  msg: string;
  header: string;
  subHeader: string;
  buttons: Array<any>;
  onDidDismiss?: () => void;
};

type ToastState = {
  color: string;
  msg: string;
  duration: number;
};

function isJsonString(str: string) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function isPriceOption(data: any): data is PriceOption {
  return (
    typeof data["name"] == "string" &&
    typeof data["price"] == "number" &&
    typeof data["general"] == "boolean" &&
    typeof data["specialist"] == "boolean" &&
    typeof data["physiotherapy"] == "boolean" &&
    typeof data["daily_room_allowance"] == "number" &&
    typeof data["daily_doctor_allowance"] == "number" &&
    typeof data["complex"] == "number" &&
    typeof data["major"] == "number" &&
    typeof data["intermediate"] == "number" &&
    typeof data["minor"] == "number"
  );
}

function parseData(data: any) {
  try {
    data["price"] = parseInt(data["price"]);
    data["daily_room_allowance"] = parseInt(data["daily_room_allowance"]);
    data["daily_doctor_allowance"] = parseInt(data["daily_doctor_allowance"]);
    data["complex"] = parseInt(data["complex"]);
    data["major"] = parseInt(data["major"]);
    data["intermediate"] = parseInt(data["intermediate"]);
    data["minor"] = parseInt(data["minor"]);
    data["general"] = data["general"] === "true";
    data["specialist"] = data["specialist"] === "true";
    data["physiotherapy"] = data["physiotherapy"] === "true";
    if (
      isNaN(data["price"]) ||
      isNaN(data["daily_room_allowance"]) ||
      isNaN(data["daily_doctor_allowance"]) ||
      isNaN(data["complex"]) ||
      isNaN(data["major"]) ||
      isNaN(data["intermediate"]) ||
      isNaN(data["minor"])
    ) {
      return 'error';
    }
    return data;
  } catch (e) {
    return "error";
  }
}

const AddPrice: React.FC = () => {
  const JSONForm = useForm<{ priceString: string }>();
  const addPriceForm = useForm<PriceOption>();
  const [alert, setAlert] = useState<AlertState | null>(null);
  const [toast, setToast] = useState<ToastState | null>(null);
  const dispatch = useDispatch();

  const [genChecked, setGenChecked] = useState(false);
  const [speChecked, setSpeChecked] = useState(false);
  const [phyChecked, setPhyChecked] = useState(false);

  const JSONSubmit = async (data: { priceString: string }) => {
    console.log("JSONSubmit: ", data);

    // check if the string is JSON before proceed further
    let isJSON = isJsonString(data.priceString);
    if (!isJSON) {
      setAlert({
        msg: "The input must be a JSON, please try again",
        header: "Cannot add price",
        subHeader: "",
        buttons: ["Close and Retry"],
        onDidDismiss: () => {
          setAlert(null);
        },
      });
      return;
    }

    let parseData = JSON.parse(data.priceString);
    // console.log("typeof", isPriceOption(parseData));
    // console.log("parseData", parseData);

    if (isPriceOption(parseData)) {
      // update global state in redux when data is integral
      dispatch(addPriceAction(parseData));
      setAlert({
        msg: "Price Added",
        header: "Data Correct",
        subHeader: "",
        buttons: ["Okay"],
        onDidDismiss: () => {
          setAlert(null);
        },
      });
    } else {
      setAlert({
        msg: "The Input is not the format of Price Option",
        header: "Wrong data",
        subHeader: "",
        buttons: ["Close and Retry"],
        onDidDismiss: () => {
          setAlert(null);
        },
      });
      return;
    }
  };

  const addPriceSubmit = async (data: any) => {
    console.log("addPriceSubmit", data);

    // checking mechanism
    if (!data.name) {
      setToast({
        color: "danger",
        msg: "Please fill in price option name",
        duration: 2000,
      });
      return;
    }

    // checking data parsable
    let newData = parseData(data);
    if (newData == "error") {
      setToast({
        color: "danger",
        msg: "Please fill again the price form (number columns)",
        duration: 2000,
      });
    } else {
      // update global state in redux when data is integral
      dispatch(addPriceAction(newData));
      setAlert({
        msg: "Price Added",
        header: "Data Correct",
        subHeader: "",
        buttons: ["Okay"],
        onDidDismiss: () => {
          setAlert(null);
        },
      });
    }
  };

  // ref: Bowtie pricing
  const placeholder = `Example:
    {
      "name":"Standard Plan",
      "price":0,
      "general":true,
      "specialist":true,
      "physiotherapy":true,
      "daily_room_allowance":750,
      "daily_doctor_allowance":750,
      "complex":50000,
      "major":25000,
      "intermediate":12500,
      "minor":5000
    }
  `;

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Add Price</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        
        {/* Method A  */}
        <Form onSubmit={JSONForm.handleSubmit(JSONSubmit)}>
          <IonList>
            <IonItemDivider>
              Method A. Enter price data below and click submit
            </IonItemDivider>
            <IonItem>
              <IonLabel>Input</IonLabel>
              <IonTextarea
                {...JSONForm.register("priceString")}
                rows={20}
                cols={20}
                placeholder={placeholder}
              ></IonTextarea>
            </IonItem>
          </IonList>
          <IonButton expand="block" type="submit" color="secondary">
            Submit
          </IonButton>
        </Form>

        {/* Method B  */}
        <Form onSubmit={addPriceForm.handleSubmit(addPriceSubmit)}>
          <IonList>
            <IonItemDivider>
              Method B. Add Price Form for non-tech user
            </IonItemDivider>
            <IonItem lines="full">
              <IonLabel position="stacked">Price option name</IonLabel>
              <IonInput
                type="text"
                placeholder="Standard Plan"
                {...addPriceForm.register("name")}
              />
            </IonItem>
            <IonItem lines="full">
              <IonLabel position="stacked">Price of the package</IonLabel>
              <IonInput
                type="number"
                placeholder="180"
                {...addPriceForm.register("price")}
              />
            </IonItem>

            <IonRow>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">General Clinic</IonLabel>
                  <IonCheckbox
                    color="secondary"
                    checked={genChecked}
                    value={`${genChecked}`}
                    onIonChange={(e) => setGenChecked(e.detail.checked)}
                    {...addPriceForm.register("general")}
                  />
                </IonItem>
              </IonCol>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Specialist</IonLabel>
                  <IonCheckbox
                    color="secondary"
                    checked={speChecked}
                    value={`${speChecked}`}
                    onIonChange={(e) => setSpeChecked(e.detail.checked)}
                    {...addPriceForm.register("specialist")}
                  />
                </IonItem>
              </IonCol>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Physiotherapy</IonLabel>
                  <IonCheckbox
                    color="secondary"
                    checked={phyChecked}
                    value={`${phyChecked}`}
                    onIonChange={(e) => setPhyChecked(e.detail.checked)}
                    {...addPriceForm.register("physiotherapy")}
                  />
                </IonItem>
              </IonCol>
            </IonRow>

            <IonRow>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Room and board (Daily)</IonLabel>
                  <IonInput
                    type="number"
                    placeholder="750"
                    {...addPriceForm.register("daily_room_allowance")}
                  />
                </IonItem>
              </IonCol>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">
                    Doctor's visit fee (Daily)
                  </IonLabel>
                  <IonInput
                    type="number"
                    placeholder="750"
                    {...addPriceForm.register("daily_doctor_allowance")}
                  />
                </IonItem>
              </IonCol>
            </IonRow>

            <IonRow>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Complex</IonLabel>
                  <IonInput
                    type="number"
                    placeholder="50000"
                    {...addPriceForm.register("complex")}
                  />
                </IonItem>
              </IonCol>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Major</IonLabel>
                  <IonInput
                    type="number"
                    placeholder="25000"
                    {...addPriceForm.register("major")}
                  />
                </IonItem>
              </IonCol>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Intermediate</IonLabel>
                  <IonInput
                    type="number"
                    placeholder="12500"
                    {...addPriceForm.register("intermediate")}
                  />
                </IonItem>
              </IonCol>
              <IonCol>
                <IonItem lines="full">
                  <IonLabel position="stacked">Minor</IonLabel>
                  <IonInput
                    type="number"
                    placeholder="5000"
                    {...addPriceForm.register("minor")}
                  />
                </IonItem>
              </IonCol>
            </IonRow>
          </IonList>
          <IonButton expand="block" type="submit" color="secondary">
            Submit
          </IonButton>
        </Form>

        <IonAlert
          isOpen={!!alert}
          header={alert ? alert.header : ""}
          subHeader={alert ? alert.subHeader : ""}
          message={alert ? alert.msg : ""}
          buttons={alert ? alert.buttons : [""]}
          onDidDismiss={alert ? alert.onDidDismiss : () => setAlert(null)}
        />
        <IonToast
          isOpen={!!toast}
          onDidDismiss={() => setToast(null)}
          message={toast ? toast.msg : ""}
          duration={toast ? toast.duration : 0}
          position="top"
          color={toast ? toast.color : "medium"}
        />
      </IonContent>
    </IonPage>
  );
};

export default AddPrice;
