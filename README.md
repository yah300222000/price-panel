## Price panel

A price page with no payment logic that provides option addition through back panel, powered by Ionic framework.


## Initialization

Package installation opens for both of NPM and Yarn.

```sh
npm install
yarn install
```

And then you can activate the app through either of the below:

```sh
ionic serve
npm start
yarn start
```


## How to add more price options

### By JSON Input

You have to input a JSON object into the textarea, with correct key and value (have type-checking)

```typescript
    {
      "name":"Standard Plan",
      "price":0,
      "general":true,
      "specialist":true,
      "physiotherapy":true,
      "daily_room_allowance":750,
      "daily_doctor_allowance":750,
      "complex":50000,
      "major":25000,
      "intermediate":12500,
      "minor":5000
    }
```

### By Form

The alternative way to add price and get rid of JSON is using the form submit.
Be ensured that the form fields are all filled, except checkbox can be left empty for false value.


## Unit Test

Test All

```sh
npm test
yarn test
```

Test Specific

```sh
npm test reducers
yarn test reducers
```